<?php
if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW'])) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="Web 6"');
    print('<h1>401 Требуется авторизация</h1>');
    exit();
}

$user = 'u21224';
$password = '2345342';
$db = new PDO('mysql:host=localhost;dbname=u21224', $user, $password, array(PDO::ATTR_PERSISTENT => true));
session_start();

$result = $db->prepare("SELECT * from admin");
$result->execute();
$flag = 0;
while ($data = $result->fetch()) {
    if ($data['login'] == $_SERVER['PHP_AUTH_USER'] && password_verify($_SERVER['PHP_AUTH_PW'], $data['pass'])) {
        $flag = 1;
    }
}
if ($flag == 0) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="For lab6"');
    print('<h1>401 Требуется авторизация</h1>');
    exit();
}
print('Вы успешно авторизовались.');
?>

<html>
<head>
    <meta charset="utf-8">
    <title>Админ</title>
    <link rel="stylesheet" media="all" href="style.css">
</head>
<body>
<h3>Введите id пользователя для удаления или изменения записи:</h3>
<form action="delete.php" method="post" accept-charset="UTF-8">
    <label>
        <input name="delete">
    </label>
    <label><input type="submit" value="Удалить запись"></label>
</form>
<form action="change.php" method="post" accept-charset="UTF-8">
    <label>
        <input name="change">
    </label>
    <label><input type="submit" value="Изменить запись"></label>
</form>
</body>
</html>

<?php
$result = $db->prepare("SELECT * from form INNER JOIN ability ON form.id = ability.form_id");
$result->execute();
print('<h3>Данные по пользователям:</h3>');
print '<table class="table" border="1" cellpadding="3" cellspacing="3">';
print '<tr>
<th>ID</th>
<th>Имя</th>
<th>E-Mail</th>
<th>Год рождения</th>
<th>Пол</th>
<th>Количество конечностей</th>
<th>Биография</th>
<th>Логин</th>
<th>Хэш пароля</th>
<th>Суперспособности</th>
</tr>';
while ($data = $result->fetch()) {
    print '<tr><td>';
    print $data['form_id'];
    print '</td><td>';
    print $data['name'];
    print '</td><td>';
    print $data['email'];
    print '</td><td>';
    print $data['year'];
    print '</td><td>';
    print $data['gender'];
    print '</td><td>';
    print $data['limb'];
    print '</td><td>';
    print $data['bio'];
    print '</td><td>';
    print $data['login'];
    print '</td><td>';
    print $data['pass'];
    print '</td><td>';
    print($data['ability_id']);
}
print '</table>';
print('* Здесь: 1 - бессмертие, 2 - невидимость, 3 - левитация');

$result = $db->prepare("SELECT COUNT(ability_id) FROM ability WHERE ability_id = '1'");
$result->execute();
$data1 = $result->fetch()[0];
$result = $db->prepare("SELECT COUNT(ability_id) FROM ability WHERE ability_id = '2'");
$result->execute();
$data2 = $result->fetch()[0];
$result = $db->prepare("SELECT COUNT(ability_id) FROM ability WHERE ability_id = '3'");
$result->execute();
$data3 = $result->fetch()[0];

print '<h3>Статистика по сверхспособностям:</h3>';
print '<table class="table" border="1" cellpadding="3" cellspacing="3">';
print '<tr>
<th>Бессмертие</th>
<th>Невидимость</th>
<th>Левитация</th>
</tr>';
print '<tr><td>';
print $data1;
print '</td><td>';
print $data2;
print '</td><td>';
print $data3;
print '</td></tr>';
print '</table>';
?>
